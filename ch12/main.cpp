#include <iostream>
#include <vector>
#include <string>
#include <stdexcept>
#include <cmath>
#include <algorithm>

const unsigned MAX_CLINDER = 5000;

std::vector<unsigned> random_series(unsigned length);
unsigned fcfs(unsigned start_position, std::vector<unsigned> series);
unsigned sstf(unsigned start_position, std::vector<unsigned> series);
unsigned scan(unsigned start_position, std::vector<unsigned> series);
unsigned c_scan(unsigned start_position, std::vector<unsigned> series);
unsigned look(unsigned start_position, std::vector<unsigned> series);
unsigned c_look(unsigned start_position, std::vector<unsigned> series);

class LookCompare
{
public:
    LookCompare(unsigned start_position) : start_position(start_position) { }
    bool operator() (unsigned a, unsigned b)
    {
        if (a < start_position && b < start_position)
            return a > b;
        else
            return a < b;
    };
private:
    unsigned start_position;
};

class CLookCompare
{
public:
    CLookCompare(unsigned start_position) : start_position(start_position) { }
    bool operator() (unsigned a, unsigned b)
    {
        if (a > start_position && start_position > b)
            return false;
        else if (a < start_position && start_position < b)
            return true;
        else
            return a > b;
    }
private:
    unsigned start_position;
};

int main(int argc, char** argv)
{
    unsigned start_position;
    if (argc != 2)
        std::cout << "usage: ./main <start_position>" << std::endl;
    try
    {
        int temp = std::stoi(argv[1]);
        if (temp < 0)
            std::cout << "the start_position should be an unsigned integer" << std::endl;
        else
            start_position = (unsigned)temp;
    }
    catch(const std::invalid_argument & e)
    {
        std::cout << "the start_position should be an unsigned integer" << std::endl;
    }

    srand(time(NULL));
    // srand(2147483648);
    std::vector<unsigned> series = random_series(1000);

    std::cout << "FCFS: " << fcfs(start_position, series) << " clinders" << std::endl;
    std::cout << "SSTF: " << sstf(start_position, series) << " clinders" << std::endl;
    std::cout << "SCAN: " << scan(start_position, series) << " clinders" << std::endl;
    std::cout << "C-SCAN: " << c_scan(start_position, series) << " clinders" << std::endl;
    std::cout << "LOOK: " << look(start_position, series) << " clinders" << std::endl;
    std::cout << "C-LOOK: " << c_look(start_position, series) << " clinders" << std::endl;
}

std::vector<unsigned> random_series(unsigned length)
{
    std::vector<unsigned> result;
    while(length--)
        result.push_back(rand() % MAX_CLINDER);
    return result;
}

unsigned fcfs(unsigned start_position, std::vector<unsigned> series)
{
    if (series.size() == 0)
        return 0;
    unsigned first = series[0];
    series.erase(series.begin());
    return abs(start_position - first) + fcfs(first, series);
}

unsigned sstf(unsigned start_position, std::vector<unsigned> series)
{
    if (series.size() == 0)
        return 0;
    unsigned closest_index = 0;
    for (unsigned i = 0; i < series.size(); ++i)
        if (abs(start_position - series[i]) < abs(start_position - series[closest_index]))
            closest_index = i;
    unsigned closest = series[closest_index];
    series.erase(series.begin() + closest_index);
    // std::cout << closest << std::endl;
    return abs(start_position - closest) + sstf(closest, series);
}

// always go to lower first
unsigned scan(unsigned start_position, std::vector<unsigned> series)
{
    std::sort(series.begin(), series.end(), LookCompare(start_position));

    unsigned clinders = 0;
    unsigned previous = start_position;
    bool isComingDown = true;
    for(unsigned i = 0; i < series.size(); ++i)
    {
        if (isComingDown && previous >= series[i])
            clinders += previous - series[i];
        else if (isComingDown && previous < series[i])
        {
            clinders += previous + series[i];
            isComingDown = false;
        }
        else if(!isComingDown && previous <= series[i])
            clinders += series[i] - previous;
        else
        {
            clinders += (MAX_CLINDER - previous) + (MAX_CLINDER - series[i]);
            isComingDown = true;
        }
        previous = series[i];
    }
    return clinders;
}

// always go to lower first
unsigned c_scan(unsigned start_position, std::vector<unsigned> series)
{
    std::sort(series.begin(), series.end(), CLookCompare(start_position));

    unsigned clinders = 0;
    unsigned previous = start_position;
    for(unsigned i = 0; i < series.size(); ++i)
    {
        if (previous < series[i])
            clinders += previous + MAX_CLINDER - 1 + (MAX_CLINDER - 1 - series[i]);
        else
            clinders += previous - series[i];
        previous = series[i];
    }
    return clinders;
}

// always go to lower first
unsigned look(unsigned start_position, std::vector<unsigned> series)
{
    std::sort(series.begin(), series.end(), LookCompare(start_position));

    unsigned clinders = 0;
    unsigned previous = start_position;
    for(unsigned i = 0; i < series.size(); ++i)
    {
        clinders += abs(previous - series[i]);
        previous = series[i];
    }
    return clinders;
}

// always go to lower first
unsigned c_look(unsigned start_position, std::vector<unsigned> series)
{
    std::sort(series.begin(), series.end(), CLookCompare(start_position));

    unsigned clinders = 0;
    unsigned previous = start_position;
    for(unsigned i = 0; i < series.size(); ++i)
    {
        clinders += abs(previous - series[i]);
        previous = series[i];
    }
    return clinders;
}