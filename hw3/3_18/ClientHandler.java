import java.net.Socket;
import java.io.InputStream;
import java.io.IOException;

class ClientHandler extends Thread {
	private Socket client;
	public ClientHandler(Socket client) {
		this.client = client;
	}

	public void run() {
		try  {
			exchangeData();
		} catch (IOException ioe) {
			ioe.printStackTrace(System.err);
		}
	}

	public void exchangeData() throws IOException {
		StreamController streamController = new StreamController();
		while (!client.isClosed() && client.isConnected()) {
			InputStream inputStream = client.getInputStream();
	        streamController.waitUntilAvailable(inputStream);
        	streamController.copyStream(inputStream, client.getOutputStream());
    	}
	}

	public void closeClient() throws IOException {
		client.close();
	}

}