import java.net.*;
import java.io.*;
import java.util.Random;

public class DateServer {
	public static void main(String[] args) {
		String[] quotes = {
			"Why is the sky such blue?", 
			"Why do all people get SSR from gacha excpet me?", 
			"I want gacha.",
			"Wrong is not me but the world",
			"You lose when you are working"
		};
		Random random = new Random();
		try {
			ServerSocket sock = new ServerSocket(6017);

			// now listen for connections
			while (true) {
				Socket client = sock.accept();
				// we have a connection
				
				PrintWriter pout = new PrintWriter(client.getOutputStream(), true);
				// write the Date to the socket
				pout.println(quotes[random.nextInt(quotes.length)]);

				// close the socket and resume listening for more connections
				client.close();
			}
		}
		catch (IOException ioe) {
				System.err.println(ioe);
		}
	}
}
