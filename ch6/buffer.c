#include "buffer.h"

buffer_item buffer[BUFFER_SIZE];
int front = 0;
int rear = 0;

int insert_item(buffer_item item)
{
	buffer[rear] = item;
	rear = (rear + 1) % BUFFER_SIZE;
	return 0;
}

int remove_item(buffer_item* outputItem)
{
	*outputItem = buffer[front];
	front = (front + 1) % BUFFER_SIZE;
	return 0;
}