#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "buffer.h"
#include <semaphore.h>

sem_t mutex;
sem_t empty;
sem_t full;

int init(int argc, char* argv[], int* sleep_time, int* producer, int* consumer);
int	str_to_int(const char* string, int* result);
void* producer(void*);
void* consumer(void*);

int main(int argc, char* argv[])
{
	int i;	 // for loop

	pthread_t* producers = NULL;
	pthread_t* consumers = NULL;
    pthread_attr_t attribute;

	int sleep_time = 0, producer_number = 0, consumer_number = 0;
	if (init(argc, argv, &sleep_time, &producer_number, &consumer_number) == -1)
		return -1;

	producers = malloc(sizeof(pthread_t) * producer_number);
	consumers = malloc(sizeof(pthread_t) * consumer_number);

	pthread_attr_init(&attribute);

	for(i = 0; i < producer_number; ++i)
    	pthread_create(producers + i, &attribute, producer, NULL);
    for(i = 0; i < consumer_number; ++i)
    	pthread_create(consumers + i, &attribute, consumer, NULL);

    sleep(sleep_time);

    for(i = 0; i < producer_number; ++i)
    	pthread_cancel(*(producers + i));
    for(i = 0; i < consumer_number; ++i)
    	pthread_cancel(*(consumers + i));
    for(i = 0; i < producer_number; ++i)
    	pthread_join(*(producers + i), NULL);
    for(i = 0; i < consumer_number; ++i)
    	pthread_join(*(consumers + i), NULL);
    free(producers);
    free(consumers);
    return 0;
}

int init(int argc, char* argv[], int* sleep_time, int* producer, int* consumer)
{
	srand(time(NULL));
	if (argc != 4)
	{
		fprintf(stderr, "usage: main <sleep_time> <producer#> <consumer#>\n");
		return -1;
	}
	if (str_to_int(argv[1], sleep_time) == -1)
	{
		fprintf(stderr, "sleep_time should be a integer\n");
		return -1;
	}
	if (str_to_int(argv[2], producer) == -1)
	{
		fprintf(stderr, "producer# should be a integer\n");
		return -1;
	}
	if (str_to_int(argv[3], consumer) == -1)
	{
		fprintf(stderr, "consumer# should be a integer\n");
		return -1;
	}
	sem_init(&mutex, 0, 10);
	sem_init(&empty, 0, BUFFER_SIZE);
	sem_init(&full, 0, 0);
	return 0;
}

int str_to_int(const char* string, int* result)
{
	if(sscanf(string, "%d", result) == 1)
		return 0;	// success
	else
		return -1;	// failure
}

void* producer(void* param)
{
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	buffer_item item;
	while(1)
	{
		sleep(rand() %  5);
		item = rand();

		sem_wait(&empty);
		sem_wait(&mutex);

		if (insert_item(item) == -1)
			fprintf(stderr, "report error condition\n");
		else
			printf("producer produced %d\n", item);

		sem_post(&mutex);
		sem_post(&full);
	}
}

void* consumer(void* param)
{
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
	buffer_item item;
	while(1)
	{
		sleep(rand() %  5);
		sem_wait(&full);
		sem_wait(&mutex);

		if (remove_item(&item) == -1)
			fprintf(stderr, "report error condition\n");
		else
			printf("consumer consumed %d\n", item);

		sem_post(&mutex);
		sem_post(&empty);
	}
}
