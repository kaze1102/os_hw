#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>

#define BUFFER_SIZE 128
#define READ_END	0
#define WRITE_END	1

long getFileSize(FILE*);
char* readFromFile(char* filePath);
void writeToFile(char* filePath, char* content);

int main(int argc, char** argv) {
	char *write_msg;
	char read_msg[BUFFER_SIZE];
	pid_t pid;
	int fd[2];

	write_msg = readFromFile(argv[1]);
	if (write_msg == NULL)
		return 1;

	/* create the pipe */
	if (pipe(fd) == -1) {
		fprintf(stderr,"Pipe failed");
		return 1;
	}

	/* now fork a child process */
	pid = fork();

	if (pid < 0) {
		fprintf(stderr, "Fork failed");
		return 1;
	}

	if (pid > 0) {  /* parent process */
		/* close the unused end of the pipe */
		close(fd[READ_END]);

		/* write to the pipe */
		write(fd[WRITE_END], write_msg, strlen(write_msg)+1); 

		/* close the write end of the pipe */
		close(fd[WRITE_END]);
	}
	else { /* child process */
		/* close the unused end of the pipe */
		close(fd[WRITE_END]);

		/* read from the pipe */
		read(fd[READ_END], read_msg, BUFFER_SIZE);
		writeToFile(argv[2], read_msg);

		/* close the write end of the pipe */
		close(fd[READ_END]);
	}

	free(write_msg);
	return 0;
}

char* readFromFile(char* filePath)
{
	FILE *file = fopen(filePath, "r");
	if (file == NULL)
	{
		fprintf(stderr, "Cannot read from %s\n", filePath);
		return NULL;
	}

	char *buffer = (char *)malloc(sizeof(char) * BUFFER_SIZE);
	if (buffer == NULL)
	{
		fprintf(stderr, "Memory allocation error\n");
		return NULL;
	}

	if (getFileSize(file) > BUFFER_SIZE)
	{
		fprintf(stderr, "File size larger than buffer size\n");
		return NULL;
	}
	
	fread(buffer, sizeof(char), BUFFER_SIZE, file);
	fclose(file);
	return buffer;
}

long getFileSize(FILE* file)
{
	long size;
	fseek(file, 0, SEEK_END);
	size = ftell(file);
	rewind(file);
	return size;
}

void writeToFile(char* filePath, char* content)
{
	FILE *file = fopen(filePath, "w");
	if (file == NULL)
	{
		fprintf(stderr, "Cannot read from %s\n", filePath);
		return;
	}

	fputs(content, file);
	fclose(file);
}