import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Arrays;

class StreamController {
    public StreamController() { }

    public void waitUntilAvailable(InputStream inputStream) throws IOException {
        while(inputStream.available() == 0)
            ; // wait until inputStream is available
    }

    public void copyStream(InputStream from, OutputStream to) throws IOException {
        byte[] buffer = new byte[512];
        int length ;
        while (from.available() > 0 && (length = from.read(buffer)) > 0) {
            to.write(buffer, 0, length);
        }
    }
}