#include <stdio.h>
#include <pthread.h>

struct threadparams
{
    double* data;
    int count;
    double result;
};

int convertToDoubles(int count, char** argv, double* numbers);
void* calculateAverage(void* parameters);
void* calculateMax(void* parameters);
void* claculateMin(void* parameters);

int main(int argc, char** argv)
{
    pthread_t averageThread, maxThread, minThread;
    pthread_attr_t attribute;

    int count = argc - 1;
    double data[count];

    struct threadparams averageThreadparams, maxThreadparams, minThreadparams;
    averageThreadparams.data = maxThreadparams.data = minThreadparams.data = data;
    averageThreadparams.count = maxThreadparams.count = minThreadparams.count = count;

    if (argc < 2)
    {
        fprintf(stderr,"usage: calculator <double value> ...\n");
        return -1;
    }

    if (convertToDoubles(count, argv, data) == -1)
        return -1;

    pthread_attr_init(&attribute);
    pthread_create(&averageThread, &attribute, calculateAverage, (void*)&averageThreadparams);
    pthread_create(&maxThread, &attribute, calculateMax, (void*)&maxThreadparams);
    pthread_create(&minThread, &attribute, claculateMin, (void*)&minThreadparams);

    pthread_join(averageThread, NULL);
    pthread_join(maxThread, NULL);
    pthread_join(minThread, NULL);

    printf("The average is %lf.\n", averageThreadparams.result);
    printf("The max is %lf.\n", maxThreadparams.result);
    printf("The min is %lf.\n", minThreadparams.result);
    return 0;
}

int convertToDoubles(int count, char** argv, double* data)
{
    int i;
    for (i = 0; i < count; ++i)
    {
        if (sscanf(*(argv + i + 1), "%lf %*s", data + i) != 1)
        {
            fprintf(stderr, "The %dth parameter is not a double", i);
            return -1;
        }
    }
    return 0; 
}

void* calculateAverage(void* parameters)
{
    struct threadparams* params = (struct threadparams*)parameters;
    params->result = 0;
    double *it;
    for(it = params->data; it < params->data + params->count; ++it)
        params->result += *it;
    params->result /= params->count;
}

void* calculateMax(void* parameters)
{
    struct threadparams* params = (struct threadparams*)parameters;
    params->result = *(params->data);
    double *it;
    for(it = params->data; it < params->data + params->count; ++it)
        if (params->result < *it)
            params->result = *it;
}

void* claculateMin(void* parameters)
{
    struct threadparams* params = (struct threadparams*)parameters;
    params->result = *(params->data);
    double *it;
    for(it = params->data; it < params->data + params->count; ++it)
        if (params->result > *it)
            params->result = *it;
}