#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

pthread_mutex_t bridge;

void* farmer(void*);

int main(int argc, char* argv[])
{
	pthread_t* north_farmers = NULL;
	pthread_t* south_farmers = NULL;

    pthread_attr_t attribute;

	int north_farmers_count = 0;
	int south_farmers_count = 0;

	int i = 0;

	// init
	if (argc != 3)
	{
		fprintf(stderr, "usage: ./main <sleep_time> <north_farmers#> <south_farmers#>\n");
		return -1;
	}

	if (sscanf(argv[1], "%d", &north_farmers_count) != 1)
	{
		fprintf(stderr, "north_farmers# should be an integer\n");
		return -1;
	}
	if (sscanf(argv[2], "%d", &south_farmers_count) != 1)
	{
		fprintf(stderr, "south_farmers# should be an integer\n");
		return -1;
	}

	north_farmers = malloc(sizeof(pthread_t) * north_farmers_count);
	south_farmers = malloc(sizeof(pthread_t) * south_farmers_count);

	pthread_attr_init(&attribute);
	pthread_mutex_init(&bridge, NULL);

	srand(time(NULL));

	// create farmers
	for(i = 0; i < north_farmers_count; ++i)
    	pthread_create(north_farmers + i, &attribute, farmer, NULL);
    for(i = 0; i < south_farmers_count; ++i)
    	pthread_create(south_farmers + i, &attribute, farmer, NULL);

    for(i = 0; i < north_farmers_count; ++i)
    	pthread_join(*(north_farmers + i), NULL);
    for(i = 0; i < south_farmers_count; ++i)
    	pthread_join(*(south_farmers + i), NULL);
    free(north_farmers);
    free(south_farmers);
    return 0;
}

void* farmer(void* param)
{

	pthread_mutex_lock(&bridge);
	printf("A farmer starts moving\n");
	sleep(rand() % 5);
	printf("The farmer finished movement\n");
	pthread_mutex_unlock(&bridge);
}