import java.net.*;
import java.io.*;
import java.util.Scanner;

public class EchoClient {
    public static void main(String[] args) {
        try {
            // this could be changed to an IP name or address other than the localhost
            Socket sock = new Socket("127.0.0.1", 6018);
            StreamController streamController = new StreamController();

            while(!sock.isClosed() && sock.isConnected()) {
                System.out.print("Enter data to transmit: ");
                streamController.waitUntilAvailable(System.in);
                streamController.copyStream(System.in, sock.getOutputStream());

                streamController.waitUntilAvailable(sock.getInputStream());
                System.out.print("echo from server: ");
                streamController.copyStream(sock.getInputStream(), System.out);
            }
            sock.close();
        }
        catch (IOException ioe) {
            ioe.printStackTrace(System.err);
        }
    }
}
