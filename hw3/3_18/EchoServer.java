import java.net.*;
import java.io.*;
import java.util.Random;

public class EchoServer {
    public static void main(String[] args) {
        try {
            ServerSocket sock = new ServerSocket(6018);
            while (true) {
                Socket client = sock.accept(); // wait until connection establish

                ClientHandler handler = new ClientHandler(client);
                handler.start();
            }
        }
        catch (IOException ioe) {
            ioe.printStackTrace(System.err);
        }
    }
}
