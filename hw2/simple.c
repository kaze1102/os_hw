#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/slab.h>

struct birthday
{
	int day;
	int month;
	int year;
	struct list_head list;
};

void add_birthday(int d, int m, int y);
void remove_all_birthday(void);

static LIST_HEAD(birthday_list);

/* This function is called when the module is loaded. */
int simple_init(void)
{
    struct birthday * ptr;

	add_birthday(1, 1, 2000);
    add_birthday(11, 11, 2011);
    add_birthday(22, 2, 2002);
    add_birthday(3, 30, 2023);
    add_birthday(3, 5, 2019);

	list_for_each_entry(ptr, &birthday_list, list)
	{
        printk(KERN_INFO "The birthday is %d/%d/%d\n", ptr->month, ptr->day, ptr->year);
	}
	return 0;
}

/* This function is called when the module is removed. */
void simple_exit(void) {
	remove_all_birthday();
}

/* Macros for registering module entry and exit points. */
module_init( simple_init );
module_exit( simple_exit );

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Simple Module");
MODULE_AUTHOR("SGG");

void add_birthday(int d, int m, int y)
{
    struct birthday *person = kmalloc(sizeof(*person), GFP_KERNEL);
    person->day = d;
    person->month = m;
    person->year = y;
    INIT_LIST_HEAD(&person->list);

    list_add_tail(&person->list, &birthday_list);
}

void remove_all_birthday()
{
    struct birthday *ptr, *next;
    list_for_each_entry_safe(ptr, next, &birthday_list, list)
    {
        printk(KERN_INFO "The deleted birthday is %d/%d/%d\n", ptr->month, ptr->day, ptr->year);
        list_del(&ptr->list);
        kfree(ptr);
    }
}